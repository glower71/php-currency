<?php

namespace Currency\Tests\Integration;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Shilov\Currency\Currency;
use Shilov\Currency\Rate;

/**
 * Class CurrencyTest
 *
 * @package Currency\Tests
 */
class CurrencyTest extends TestCase
{
    protected $currency = null;

    public function setUp(): void
    {
        $this->currency = new Currency();
    }

    public function tearDown(): void
    {
        $this->currency = null;
    }

    public function testAddProviders()
    {
        $todayRate = $this->currency->getAvg('USD', 'RUR', Carbon::today());
        $this->assertContainsOnlyInstancesOf(
            Rate::class,
            [$todayRate]
        );
    }
}