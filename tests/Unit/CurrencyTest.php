<?php

namespace Currency\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Shilov\Currency\Currency;
use Shilov\Currency\Exception\InvalidProviderException;

/**
 * Class CurrencyTest
 *
 * @package Currency\Tests
 */
class CurrencyTest extends TestCase
{
    protected $currency = null;

    public function setUp(): void
    {
        $this->currency = new Currency();
    }

    public function tearDown(): void
    {
        $this->currency = null;
    }

    public function testAddProviders()
    {
        $this->expectException(InvalidProviderException::class);

        $this->currency->addProviders([
            'DummyClass'
        ]);
    }
}