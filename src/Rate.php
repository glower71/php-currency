<?php

namespace Shilov\Currency;

class Rate
{
    protected $currencyFrom;
    protected $currencyTo;
    protected $rate;

    public function __construct(string $currencyFrom, $currencyTo, float $rate)
    {
        $this->currencyFrom = $currencyFrom;
        $this->currencyTo = $currencyTo;
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getCurrencyFrom()
    {
        return $this->currencyFrom;
    }

    /**
     * @param  mixed  $currencyFrom
     */
    public function setCurrencyFrom($currencyFrom): void
    {
        $this->currencyFrom = $currencyFrom;
    }

    /**
     * @return mixed
     */
    public function getCurrencyTo()
    {
        return $this->currencyTo;
    }

    /**
     * @param  mixed  $currencyTo
     */
    public function setCurrencyTo($currencyTo): void
    {
        $this->currencyTo = $currencyTo;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param  mixed  $rate
     */
    public function setRate($rate): void
    {
        $this->rate = $rate;
    }

    public function __toString()
    {
        return $this->currencyFrom.' '.$this->currencyTo.' '.$this->rate;
    }
}