<?php

namespace Shilov\Currency\Providers;

use GuzzleHttp\Client;
use Shilov\Currency\BaseProvider;
use Shilov\Currency\Exception\RateNotFoundException;
use Shilov\Currency\Exception\UnavailableProviderException;
use Shilov\Currency\Rate;

class RBCProvider extends BaseProvider
{
    protected const URL = 'https://cash.rbc.ru/cash/json/converter_currency_rate/';

    public static function getServiceName(): string
    {
        return 'cbr';
    }

    protected function parseData(array $params, array $data): Rate
    {
        if (isset($data['data'])) {
            return new Rate($params['currencyFrom'], $params['currencyTo'], $data['data']['rate1']);
        }

        throw new RateNotFoundException();
    }

    public function getData(array $params)
    {
        $client = new Client();

        $request = [
            'currency_from' => $params['currencyFrom'],
            'currency_to' => $params['currencyTo'],
            'source' => 'cbrf',
            'sum' => 1,
            'date' => $params['date']->format('Y-m-d')
        ];

        $json = $client->get(self::URL, ['query' => $request])->getBody()->getContents();
        try {
            $data = json_decode($json, true);
        } catch (\Exception $exception) {
            throw new UnavailableProviderException();
        }

        return $data;
    }
}