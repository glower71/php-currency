<?php

namespace Shilov\Currency\Providers;

use GuzzleHttp\Client;
use Shilov\Currency\BaseProvider;
use Shilov\Currency\Exception\RateNotFoundException;
use Shilov\Currency\Exception\UnavailableProviderException;
use Shilov\Currency\Rate;

class CBRProvider extends BaseProvider
{
    protected const URL = 'http://www.cbr.ru/scripts/XML_daily.asp';

    public static function getServiceName(): string
    {
        return 'rbc';
    }

    protected function parseData(array $params, array $data): Rate
    {
        foreach ($data['Valute'] as $item) {
            if ($item['CharCode'] == $params['currencyFrom']) {
                $rate = str_replace(',', '.', $item['Value']) / $item['Nominal'];
                break;
            }
        }

        if(isset($rate)) {
            return new Rate($params['currencyFrom'], $params['currencyTo'], $rate);
        }

        throw new RateNotFoundException();
    }

    public function getData(array $params)
    {
        $client = new Client();

        $request = [
            'date_req' => $params['date']->format('d/m/Y'),
        ];

        $content = $client->get(self::URL, ['query' => $request])->getBody()->getContents();
        try {
            $data = simplexml_load_string($content);
            return json_decode(json_encode($data), TRUE);
        } catch (\Exception $exception) {
            throw new UnavailableProviderException();
        }
    }
}