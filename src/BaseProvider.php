<?php

namespace Shilov\Currency;

use Carbon\Carbon;

abstract class BaseProvider
{
    abstract public static function getServiceName(): string;

    abstract protected function getData(array $params);

    abstract protected function parseData(array $params, array $data);

    /**
     * @param $currencyFrom
     * @param $currencyTo
     * @param  Carbon  $date
     * @return Rate
     */
    public function get($currencyFrom, $currencyTo, Carbon $date): Rate
    {
        $params = [
            'currencyFrom' => $currencyFrom,
            'currencyTo' => $currencyTo,
            'date' => $date,
        ];

        $data = $this->getData($params);

        return $this->parseData($params, $data);
    }
}