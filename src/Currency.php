<?php

namespace Shilov\Currency;

use Carbon\Carbon;
use Shilov\Currency\Exception\InvalidProviderException;
use Shilov\Currency\Exception\NoProvidersException;
use Shilov\Currency\Exception\ProviderNotFoundException;
use Shilov\Currency\Providers\CBRProvider;
use Shilov\Currency\Providers\RBCProvider;

class Currency
{
    protected $providers = [];

    /**
     * Currency constructor.
     * @throws InvalidProviderException
     */
    public function __construct()
    {
        $this->registerBaseProviders();
    }

    /**
     * @throws InvalidProviderException
     */
    protected function registerBaseProviders()
    {
        $this->addProviders([
            new CBRProvider(),
            new RBCProvider(),
        ]);
    }

    /**
     * @param  array $classes
     * @throws InvalidProviderException
     */
    public function addProviders(array $classes)
    {
        foreach ($classes as $class) {
            /**
             * @var BaseProvider $class
             */
            if (!is_subclass_of($class, BaseProvider::class)) {
                throw new InvalidProviderException();
            }

            $this->providers[$class::getServiceName()] = $class;
        }
    }

    /**
     * @param  BaseProvider  $class
     */
    public function deleteProvider(BaseProvider $class): void
    {
        unset($this->providers[$class::getServiceName()]);
    }

    /**
     * @param $providerName
     * @param $currencyFrom
     * @param $currencyTo
     * @param  Carbon  $date
     * @return Rate
     * @throws ProviderNotFoundException
     */
    public function get($providerName, $currencyFrom, $currencyTo, Carbon $date): Rate
    {
        /**
         * @var BaseProvider $provider
         */
        if (isset($this->providers[$providerName])) {
            $provider = $this->providers[$providerName];
        } else {
            throw new ProviderNotFoundException();
        }

        return $provider->get($currencyFrom, $currencyTo, $date);
    }

    /**
     * @param $currencyFrom
     * @param $currencyTo
     * @param  Carbon  $date
     * @return Rate
     * @throws NoProvidersException
     * @throws ProviderNotFoundException
     */
    public function getAvg($currencyFrom, $currencyTo, Carbon $date): Rate
    {
        if (count($this->providers) == 0) {
            throw new NoProvidersException();
        }

        $sum = 0;
        foreach (array_keys($this->providers) as $providerName) {
            $rate = $this->get($providerName, $currencyFrom, $currencyTo, $date);
            $sum += $rate->getRate();
        }

        return new Rate(
            $currencyFrom,
            $currencyTo,
            $sum / count($this->providers)
        );
    }
}